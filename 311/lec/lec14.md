# lec14

IP Address: 32 bit identifier for a host, router interface
The interface is that part of a router that connect between a host router and the physical link.

## Global Addresses

3 Types of Formats for the addresses

1. Type A: 

	* Lower 3-bytes are clobbered

2. Type B:

	* Lower 2-bytes are clobbered

3. Type C:

	* Lower 1-byte is clobbered

## Subnets

If we split the ip address into two (parts) 16-bit portions:

* subnet - high bits
* host  part - low order bits

### Subnet mask

usually we'll have `x.x.x.x/mask` where the mask is also some 1-byte value.

We can determine the class of a subnet from `mask` value, hereby referred to by subnet mask.
First we split the 32-bit mask into two portions of 1's and 0's respecctively.

Example: `255.240.0.0` = `11111111 11110000 00000000 0000000`

In our example the split happens at offset 12, meaning our x value from before in this case is just 12.

## Addressing: CIDR

Classless InterDomain Routing

This says that any address say: `a.b.c.d./x` will tell us that x is the bit offset in the subnet portion of an address.


