# lec6

> diagrams needed for this section

## Transistors

Two types of transistors will be discussed here: `NPN` and `PNP`.
Both types however, do share some properties here by referred to as the following:

* E = Emitter
* B = Base
* C = Collector

### NPN

![](../img/lec6npn.png)

Current will __not__ flow  across collector through base into emitter if do nothing.
When we apply a current to the base then current flows through the transistor.

### PNP 

![](../img/lec6pnp.png)

Current will flow across emitter through base into collector if we don't do anything.
When we apply a current to the base then current flow stops.

