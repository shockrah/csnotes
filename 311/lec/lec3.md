# lec3

## Connecting Networks

Instead of having every network to every other network, we have global ISP's which connect and close the gaps between varying networks.
On top of this there are IXP's(internet exchange point) where each of these global ISP's connect to each other.
Sometimes links from one Global ISP to another may be referred to as a perring link.

## Delay, loss Throughput

> Delay

For delay there are 4 main sources of delay: processing delay, queueing delay, transmission delay, propagation delay.
All of this combined results in the nodal processing delay.

> Loss

Loss of packets in the network isn't something we should ever be surprised about since it happens all the time. This is especially true when you have one node with too much traffic being pushed onto it.
Typically we have some methods of telling the sender that some sent packet was dropped. 
These packets are usually tiny enough where they shouldn't get dropped but then if those get dropped we can keep stacking the dropped information along the network.

> Throughput

Rate at which bits are exchanged from sender to receiver.
It's not uncommon to have to links connected which have different throughput rates which means in these scenarios we have a _bottleneck link_.

## Layering Network Protocols

_Naming a few layers of the network stack_

### Application Layer 

Software that usually manipulates data the most on a particular machine.

> FTP, SMTP, HTTP

### Transport

Protocols here setup for what kind of data we have and essentially package the data to be ready to be sent.

> TCP UDP

### Network

Here we are actually sending things around from machine ot machine along some physical network, be it wirelessly or in a wired fashion.

> IP routing protocols

### Link

Data transfer bewtween machines in a physical network.

> Ethernet, 802.11x

### Physical 

Wires yo
