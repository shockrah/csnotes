# Hashing

Some things to deal with in hashing:
	* collisions
	* deterministic behavior
	* speed

# Collisions

## Chaining Buckets

Say a list where each item in the list is a really a sub-list.
Each item in those sub-lists are the actual data we are trying to store

# Radix Sort

Given a set `s` we can of length `n` we can insert each item `i` into a set where we use chaining to handle collisions.
Insertion strategy: pull smallest digit from `i` to select the bucket, increasing the digit from each number for each iteration.

Time complexity: O(nk)

Space Complexity: O(n)

The problem with radix is really that we can only sort integers
There is also the notion of chaining which isn't to great for sets of data where there is _going_ to be tons of collision.
