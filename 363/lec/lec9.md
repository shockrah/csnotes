# lec9

## Lab

This lecture has a corresponding lab activity in `lab/`, the instructions are named `views-lab.pdf` and the second one is `contraints-lab.pdf`.

## Views

```
create view newTabelName as select ... from targetTable;
```

This will create a `view` which whenever it is queried will pull data from some base table.
Really the `view` is a kind of "_macro_" which is stored in a `catalog` that normal, non-admin users can use to access a database.
The catalog is saved in a table somewhere in the database.
Think of this catalog like a container(_table_) for the other tables in the database.

### Pros & Cons

Problems:

* Computing the view multiple times can be expensive
* Maintainence

There are two strategies to dealing with the second item: eager and lazy strategies.

1. Eager
	* If the target table of some view changes the update the view immediately
2. Lazy
	* Don't update the view until it is needed(_queried_)


## Check Contraint

Checks values when they are inserted to validate their legitimacy.

```
create table blah(
	id varchar(8) check (id like "%-%"),
);
```
This is how we can avoid accidently putting in null or downright logically incorrect data into a table.

We can also require entries be unique as well.

```
create table blah (
	dept_name varchar(20),
	...
	unique(dept_name)
);
```
_KEEP IN MIND HOWEVER_. With `unique()` if we try to check if a new entry is unique it will always fail with NULL since operations with NULL results in false.
That means we will be able to insert NULL values into the table even if they are not unique.
