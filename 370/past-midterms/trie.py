class Node:
    def __init__(self, leaf=False):
        # refs list of next valid characters
        self.letters = [None] * 26
        self.isLeaf = leaf


class Trie:
    def __init__(self):
        self.root = Node()

    def _charIndex(self, char):
        return ord(char) - ord('a')

    def insert(self, string):
        curr = self.root
        for i in string:
            refIndex = self._charIndex()
            # create nodes if we have to 
            if curr.letters[refIndex] is None:
                curr.letters[refIndex] = Node()

            curr = curr.letters[refIndex]
        # Set the last letter to be a leaf
        curr.isLeaf = True

    def remove(self, string):
        # Lazy removal
        curr = self.root
        for i in string:
            refIndex = self._charIndex(i)
            if curr.letters[refIndex] is None:
                break
            curr = curr.letters[refIndex]
        curr.isLeaf = False


    # We're also going to be printing things as we go along the thing
    def lookup(self, string):
        curr = self.root
        for i in string:
            refIndex = self._charIndex(i)
            # Make sure we don't walk into nothing
            if curr.letters[refIndex] is None:
                return False
            curr = curr.letters[refIndex]
            print(i, end='')

        return curr.isLeaf

    def all(self, node, word):
        if node is None:
            return

        if node.isLeaf:
            # Print out the word we have so far
            self.lookup(word)


