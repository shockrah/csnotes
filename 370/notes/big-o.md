# Calculating Big O

Suppose we have the following function

```
func(x):
	if cond1 
		O(n^2)
	elif cond2
		O(n)
	else 
		O(1)
```

How do we define the _average_ time complexity:
* any input which isn't setup to be _special_
	1. linear search which always finds the first elementO(1)
	2. linear search that doen't find anything/last elementO(n)

With linear searches: finding an average item in the middle(somwhere) we have to cover: 
	* x% of the n size list

Since we drop the constants in time complexity expressions we thus end up with O(n).

## Heap Sort

Time Complexity Exercise

* Best: 
* Average:
* Worst:
* Space:

## Insertion Sort

Time Complexity Exercise

* Best: 
	* Already sorted meaning we go through the whole list once 
	* O(n) - because we'll do the check into our left sub-list but we don't go into it leaving us as constant time spent per item ie c\*n

* Average:
	* Pretty close to n^2 just for the sake of 
	* O(n^2)

* Worst:
	* Going through the whole list sorting everything(backwards)
	* O(n^2)

		* must go through N items for each item 
* Space:

# Notation

Time Complexity: O(n)

Space Complexity: OMEGA(n)

Both Complexity: THETA(n)
	* such a complexity only exists if O(n) == OMEGA(n)
