import java.util.Scanner;

public class Hangman {
	// addes a new found character to the string
	public static void newChar(String source, char[] target, char c) {
		// putting a new char into the target string
		for(int i =0; i<source.length(); i++) {
			// only pass through the matches in the string
			if(source.charAt(i) == c) {
				target[i] = c;
			}
		}
	}

	// deals with normal guesses 
	// only given good input
	public static void guessHandler(char c, char[] prog, String source) {	
		boolean found = false;
		// looking to see where we need to change the progressive array
		for(int i =0;i<source.length();i++) {
			if(source.charAt(i) == c) {
				prog[i] = c;
				found = true;
			}
		}
		if(found) {
			System.out.println(c+ " is in the word");
			System.out.println("Progress so far: " + new String(prog));
		}
		else {
			System.out.println(c+ " is not in the word");
		}
	}

	// checks if we're given an alphabet character
	public static boolean alphaChar(char c) {
		if(c < 'A') {
			return false;
		}
		if(c>'Z' && c<'a') {
			return false;
		}
		if(c > 'z') {
			return false;
		}
		return true;
	}

	// Returns a new character to put into the word
	public static char useHint(String prev, String full) {
		for(int i =0;i<prev.length()-1;i++) {
			if(full.contains(prev.charAt(i) + "")) {
				continue;
			}
			else { 
				return prev.charAt(i);
			}
		}
		return '\0';
	}

	public static void main(String[] args) {

		System.out.println("\tHangman");
		System.out.print("Enter a word: ");

		// read stdin
		Scanner scan = new Scanner(System.in);
		String raw = scan.nextLine();

		// keep track of the player's progression
		char[] progArr = new char[raw.length()];
		StringBuilder guessChars = new StringBuilder();

		// ignore spaces by setting them to '#'
		for(int i =0;i<raw.length();i++) {
			if(raw.charAt(i) == ' ') {
				progArr[i] = '#';
			}
			else {
				progArr[i] = '_';
			}
		}

		// bannering
		System.out.println("So far the word is :" + new String(progArr));
		System.out.println("You have 4 guesses remaining.");

		int guessCount = 4;
		// Dealing with main game loop here
		while(true) {
			System.out.print("Enter either 1 for guessing or 2 for hint: ");
			String attempt = scan.nextLine();

			if(attempt.contains("1")) {
				System.out.print("Enter your guess: ");
				attempt = scan.nextLine();
				// if we have good input we can go through a normal check
				if(alphaChar(attempt.charAt(0))) {
					guessHandler(attempt.charAt(0), progArr, raw);
					guessChars.append(attempt.charAt(0));
					System.out.println("Characters used: " + guessChars.toString());
				}
			}
			else if(attempt.contains("2")) {
				if(guessCount == 0) {
					System.out.println("No more guesses/hints left!");
					continue;
				}
				guessCount--;
				System.out.println(guessCount + " guesses left.");


				// find a fresh character
				char hint = useHint(guessChars.toString(), raw);
				guessHandler(hint, progArr, raw);
				// adjust the running track 
				guessChars.append(hint);
				System.out.println("Characters used: " + guessChars.toString());
			}
			else {
				System.out.println("Invalid input");
			}

			// exit condition
			if(new String(progArr).contains("_")) {
			if(progArr.toString().contains("_")) {
				continue;
			}
			else {
				System.out.println("Congrats!");
				return;
			}
		}
	}
}
}
