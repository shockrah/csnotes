# Shortest Path

_Informed searches_ vs _uninformed searches_

# Heuristical Functions

The _good enough_ approach.
Just like traversing grids graphs need some kind of weights on them for us to use a _good enough_ algorithm.

Keep in mind that this doesn't really guarantee a _good_ solution, just one that can be determined quickly.

# Dijkstra's

Both the informed and uninformed versions work off the weights to find the shortest path.
We do this by spamming the shortest sub-routes first and adding them up, this gives back the shortest global path in the graph.

