# lec7 

## Transport Layer

Transport protocols usually lie in an end node's operating system.

Send side:

* Here we break the messages into _segments_, then we pass that segment into the network layer.

Receive side:

* Reassembles the segments into messages then passes it off to the application layer.

If the network layer is communication then the transport layer is essentially running processes talking to each other on different nodes.
Keep in mind as well that we don't concern ourselves with the responsibilities of other layers.

## Transport Layer Protocols

* TCP

Reliable, in-order delivery. Also provides congestion and flow control, but requires connections to be opened.

* UDP

_No frills_ extension 


Two things that both of the above do not offer however

* Delay guarantees
* Bandwidth guarantees

## Multiplexing and Demultiplexing

### Multi

Handle data from multiple sockets, add transport header 

For connectionless oriented multiplexing the segments must contain both the source and destination port numbers.
However, conecction oriented multiplexing we need source/destination IP and port numbers.

### Demultiplexing

Host receives IP datagrams:

* Each DG has soure IP and destination IP
* Each DG carries one transport-layer segment
* Each segment source and destination port number

There is also connectionless de-multiplexing

* Host must check destination port # in the given segment
* It then passes that segment to the socket with that port number

If we have nultiple segments with the same destination socket then they just get sent to that segment in some fashion. 

__TCP__ sockets are identified by a tuple of 4 items:

* source IP
* source PORT
* destination IP
* destination PORT

The receiver uses this tuple to direct segments to the proper socket.
For something like a webserver this also means that creating these sockets will mean there is some overhead as well which usually means we maintain sockets for some time to avoid that overhead. 
The benefit to this however is that with open connections we are able to serve content more quickly.


## UDP Segment

* Header 

64 bits of length

Lower 16 bits of the header is dedicaated to the check sum is gets verified on the receiver's end.
Lower 16 bits is essentially an addc checksum with a not at the end.
