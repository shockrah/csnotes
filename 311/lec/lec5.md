# lec5

## Server States

> Cookies

Gives us the ability to create the concept of _state_.
Even though the session may end the cookie can be store in both the cient's machine and on the server.

## Web caching

Instead of directly going from `client machine --> requested server`, we would go from `client machine --> proxy server --> requested server`
Proxy machines of course must act as both clients and servers. 

## SMTP

Three main components to SMTP

* user agents
* mail servers
* simple mail transfer protocol

### Mail

Each user has a _mailbox_ somewhere on the network.
Each server machine must also be able to hold large amounts of data within their queue's to hold onto _outgoing_ messages. 

Sending mail over SMTP


1. create some message
2. user1 sends their message to their mail server
3. mail server opens tcp connection with target mail server
4. targget mail server receives the data over TCP connection
5. target mail server dumps that message into the proper mailbox
6. target useragent then request's from their mail server for any new incoming messages.

It's important to note that SMTP has no security built in which means it's beyond trivial to spoof addresses and messages to mail server.
Modern day mail has security built on top of SMTP or often they use newer more modern infrastructures to actually deal with mail in a more secure fashion.

## DNS: domain name system

Firstly we need some way of identifying others.
For humans we use www.website.xyz but machines will typically use 32-bit addreses for _datagrams_.

### Services

* hostname to IP address translation
* host aliasing
* mail server aliasing
* load distribution
	* many ip's will funnel down to a single addres

### Root Servers

Globally there are 13 logical root name _servers_ worldwide, however, they are replicated multiple times to ensure that they never go down.

Even though individual networks can have their own dns servers there are also _glbal_ ones which are typically up and functioning since the whole of the internet uses these servers to resolve hostnames.

Now let's take a look at name resolution:

* Client requests website.xyz
* Request is sent to local dns server. 
* Local DNS sends that request to the authoritative or root server with the knowledge of that type of connection.

Typicaclly we'll look at the `.xyz` portion of the site so that the DNS server's to whom to communicate with to make request fulfilled.

## Peer to Peer

Here we don't have any always on server, instead we have a network in which requests are sent and transferred accordingly.
The advantage of this is that the larger the p2p network then the faster speeds to transfer files between nodes gets faster and faster since there is less chance for throttled bottle necks to actually happen.

The users in a p2p network would usually not just request data but the main tax for being in that network is that you must also uploaded file streams and allowing other people's data to be sent over their machine.

