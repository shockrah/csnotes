# lec26

## Some types of Database Structures

1. Key-Values

Just like a standard map, we provide a `key` and get a `value`.
Maks things easy for usage but recall that our hash function is really delicate. 
Because we're using a map or a set type of container.

2. Document

Welcome to json/xml world.
Now we just look for documents(json/xml) instead of looking for some data with a predefined structure.

3. Column Family 

Variant of key/value but this time we store things in columns instead of rows.
Advantage here is that we can quickly search through columns for analysis type things.

4. Graph

Data is a _graph_(wow).

We still have some key-value system to find a node in the graph but we can create edges between values to create relationships.

## NoSQL

### In favor of usage

* Data is not uniform
* Dataset is massive 

### Against 

* You need consistency 
* 
