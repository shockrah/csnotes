# lec11

## TCP Congestion CControl

Additive increase & Multiplicative decrease

Sender increases window size until a loss happens.
	* Additively: increase by 1 every round-trip-time until a loss is detected.
	* Multiplicative decrese: cut window size in half

Sender limits the transmission by changing the size of it's congestion window.
The sender detects loss with one of two methods:
	* Timeout
	* Triple Duplicate ACK's

## General Fairness

### TCP Fairness

Say we have two competing sessions:
	* additive increase slope of 1, as throughput increases
	* multiplicative decrease lowers throughput proportionally

### UDP Fairness

Certain types of applications won't be be rate limited by TCP fairness.
Streaming video for instance won't since we just want to _throw_ data across as much as possible.
This also means we have to account for loss and tolerate it when it does happen because UDP doesn't account for loss anyway.

