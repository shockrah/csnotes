# Blockchain fundamentals

data within the chain is _immutable/tamper evident_

Block chain is also a distrubuted ledger system where everyone has a copy of a shared database.


## Bank Model v Decentralized Model

### Bank

* Identity management: Accounts basically

* Services: Transaction service

* Record Management: Transaction history 

* Trust: assigned entity to trust in v a consensus protocol


## Identity

Private/Public key pairs must be gen'd to properly communicate

## Transactions

> When is a transaction valid

1. Signature
2. available funds
3. No other transactions are using the funds

Say an account has 10$ and tries sending 10$ to 2 other accounts at the same time. We need to make sure we can't send that money twice, only once or none at all.

Bank Model: transactions happen sequentially so the above scenario doesn't ever happen as long as the central agent atomizes each transaction; that way they happen sequentially.

Keeping track of unspent money: **UTXO Model**

A UTXO is basically like a piggy bank within someones account but the money can only be spent all at once.

Example:
```
Gloria has: { 5 2 1 }
Brian has:  { 2 4 }
```
If Gloria wanted to send 4 coins to Brian she would have to use the utxo of 5 and send 4 coins to Brian and 1 to herself.
In this way we spend all the money in that UTXO.


The motivation behind this method is that its very easy to code without much hassle.

Once we have a set of transactions that we've done we put them into a block(to avoid spamming transaction data onto the network).
We then just periodically put this onto the block chain.

## Persistence

Instead of one database hosted by one person we actually let _everyone_ on the network store a copy of the block chain.



## Proof of Work

* Using consensus

People propse transactions/blocks and the rest vote on whether or not they will add those transactions to their copies of the block chain.

Everyone then looks at the transactions and makes sure that they are valid.
This way we can avoid double spending.
The issue is that if they
