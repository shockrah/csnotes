class Trie:
    def __init__(self, *words):
        self.root = {}

        for word in words:
            self.insert(word)

    def insert(self, word):
        curr = self.root
        for c in word:
            if c not in curr:
                curr[c] = {}
            else:
                curr = curr[c]

        # add a marker to show that we are a leaf
        curr['.'] = '.'
        self.root = curr

    def remove(self, word):
        pass

    def printWord(self, word):
        pass

    def all(self):
        pass
