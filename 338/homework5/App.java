/*
Author: Alejandro Santillana
Description: Builds a basic school data tracker
	keeps track of students and what course they are enrolled in as well as what courses
	are offered by the school.
	Course information is also available
	Input is read from file
*/
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.layout.GridPane;
import javafx.scene.control.ListView;

public class App extends Application {
	static public School school_;
	
	// next 3 methods are just here to populate the scene with items insede them
	public Scene setStudentScene(Stage stage, Scene _prevScene) {
		GridPane _layout = new GridPane();
		// make the buttons 
		Button addButton = new Button("Add student");
		Button returnButton = new Button("Return");
		// adding buttons to the scene's layout
		_layout.add(addButton, 1,0,1,1);
		_layout.add(returnButton, 1,1,1,1);

		// add event listeners for the buttons
		returnButton.setOnAction(e-> stage.setScene(_prevScene));
		addButton.setOnAction(e->stage.setScene(Input.studentInputScene));

		// add a responsive view to the for the student list
		ListView<Student> dataView = new ListView<Student>(school_.uiGetStudents());
		_layout.add(dataView, 1,3,1,1);

		// finally return a new scene with the finished layout
		Scene ret = new Scene(_layout, 1280, 720);
		return ret;
	}
	public Scene setInstructorScene(Stage stage, Scene _prevScene) {
		GridPane _layout = new GridPane();
		// buttons for this page
		Button addButton = new Button("Add instructor");
		Button setButton = new Button("Assign to course");

		Label inIdLabel = new Label("Instructor ID: ");
		TextField inIdField = new TextField();
		Label coIdLabel = new Label("Course ID: ");
		TextField coIdField = new TextField();

		Button returnButton = new Button("Return");

		// setup buttons
		_layout.add(addButton, 1,0,1,1);
		_layout.add(setButton, 1,1,1,1);

		_layout.add(inIdLabel, 2,1,1,1);
		_layout.add(inIdField, 3,1,1,1);
		_layout.add(coIdLabel, 4,1,1,1);
		_layout.add(coIdField, 5,1,1,1);

		_layout.add(returnButton, 1,2,1,1);

		// add responsive view of instructor list
		ListView<Instructor> dataView = new ListView<Instructor>(school_.uiGetInstructors());
		_layout.add(dataView, 1,3,1,1);

		addButton.setOnAction(e -> stage.setScene(Input.instructorInputScene));
		// i really don't want another scene cuz time frames
		setButton.setOnAction(e -> Input.setInstructor(inIdField.getText(), coIdField.getText()));
		returnButton.setOnAction(e -> stage.setScene(_prevScene));

		Scene ret = new Scene(_layout, 1280, 720);
		return ret;
	}
	public Scene setCourseScene(Stage stage, Scene _prevScene) {
		GridPane _layout = new GridPane();

		Button addButton = new Button("Add course");
		Button deleteButton = new Button("Delete course");
		TextField courseField = new TextField();
		Button returnButton = new Button("Return");

		_layout.add(addButton, 1,0,1,1);
		_layout.add(deleteButton, 1,1,1,1);
		_layout.add(courseField, 2,1,1,1);
		_layout.add(returnButton, 1,2,1,1);

		addButton.setOnAction(e -> stage.setScene(Input.courseInputScene));
		deleteButton.setOnAction(e -> Input.courseDeletion(courseField.getText()));
		returnButton.setOnAction(e -> stage.setScene(_prevScene));

		// add response data view for course list
		ListView<Course> dataView = new ListView<Course>(school_.uiGetCourses());
		_layout.add(dataView, 1,3,1,1);

		Scene ret = new Scene(_layout, 1280, 720);
		return ret;
	}
	@Override
	public void start(Stage stage) {
		// make the initial menu with three buttons
		stage.setTitle("School App");

		// Main menu comes first
		GridPane grid = new GridPane();
		Button instButton = new Button("Instructors");
		Button studButton = new Button("Students");
		Button courseButton = new Button("Courses");
			// add those buttons to the main_menu scene
		grid.add(instButton, 1, 0, 1, 1);
		grid.add(studButton, 1, 1, 1, 1);
		grid.add(courseButton, 1, 2, 1, 1);

		Scene main_menu = new Scene(grid, 1280, 720);

		// next we can create the other menu's 
		Scene studMenu = setStudentScene(stage, main_menu);
		Scene instMenu = setInstructorScene(stage, main_menu);
		Scene courMenu = setCourseScene(stage, main_menu);

		// setup form pages
		Input.initStudentInput(studMenu, stage);
		Input.initInstructorInput(instMenu, stage);
		Input.initCourseInput(courMenu, stage);

		// now we can add the event listeners for our main menu as the other scenes now exist
		instButton.setOnAction(e-> stage.setScene(instMenu)); 
		studButton.setOnAction(e-> stage.setScene(studMenu)); 
		courseButton.setOnAction(e-> stage.setScene(courMenu)); 

		stage.setScene(main_menu);
		stage.show();
	}
	public static void main(String[] args) {
		// First we'll create a school
		School.createSchool("CSUMB");
		school_ = School.getSchool();
		// Next we'll populate it with some data
		school_.readData("data1.txt");
		school_.readData("data2.txt");
		// Next we'll create the UI
		launch();
	}
}
