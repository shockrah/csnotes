# Space complexity

With function calls there is explicit space used.

Implicit space used is about how much work is done to take up space 

# Binary recursive search

> Explicit space: O(c)

This is the amount of space taken per call.
We know how much space has to be allocated every call so we can say it would likely be constant

> Implicit: O(log(n))

Because again we will likely take up log(n) times to reach our destination an this comes from our rekked stackspace.
