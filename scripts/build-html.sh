mkdir -p public/img
cp gitlab-page/style.css public/style.css

md() {
	pandoc -s --css style.css `ls -v $1`
}

md "311/lec/*.md" 	> public/intro-to-networking-311.html
md "312/*.md" 		> public/network-security-concepts-312.html

md "337/lec/*.md" 	> public/computer-architecture-337.html
cp 337/img/* 		public/img/

md "363/lec/*.md" 	> public/intro-to-databases-363.html
md "370/notes/*.md" 	> public/advanced-algorithms-370.html
md "412/*.md"			> public/network-administration-412.html

md gitlab-page/index.md > public/index.html
