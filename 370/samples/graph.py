#!/usr/bin/python3

import random
class Node:
    def __init__(self, data:int):
        self.data = data
        # empty list o rama
        self.refs = []

    def __str__(self):
        return f'{self.data}:{self.refs}'

def new_node(data):
    """
    Provides a new node for a ref list or a basic s
    """
    return Node(data)

def add_node(base, data):
    """
    Creates a new node for the base node to hook onto
    """
    base.refs.append(new_node(data))

def show_children(node):
    """
    LMAO @ your life
    """
    print(node.refs)

def populate(node):
    """
    Populate our node with some data
    """
    for i in range(1,random.randint(2,10)):
        new_tmp = new_node(random.randint(0,100))
        node.refs.append(new_tmp)

def child_count(node):
    print(f'{len(node.refs)}')

if __name__ == "__main__":
    root = Node(0)
    for i in 'ABCDE':
        add_node(root, i)

    print(f'Children count of root: {len(root.refs)}')
    for i in root.refs:
        populate(i)
        child_count(i)

    print('Root node:')
    show_children(root)
