# lec6

## Content Distribution Networks

> tdb
## Sockets

### Connections

Any time we setup some type of connection there's also some handshake between both nodes between client & server.
With `UDP` however there is _no_ concept of a connection. 
Instead we send data directly to an address.

With TCP connections however we do assume the concept of a connection which means we have to have that handshake between client and server in order to anything with the target server.
This handshake creates a socket on the server and on the client machine through which both can communicate.
