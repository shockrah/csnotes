# lec23

_More on stuff about building a usable model for given data; more specifically BCF(Boyce Codd Normal Form)_

BCNF : any table where the are no redundancies based on functional dependancies

> 

## Lossless Decomposition

If we split a table that isn't in BCN form so that the new tables are in BCN form we should be able to natural join them back to the original state.

## Normalization 3: Third Normal Form

Take everything out from the original table which is part of a functional dependancy.

Example:

original: `id name office` {id -> name}

Table 1: `id name` [functional dependancy participants]

Table 2: `id office` [everything else + root of FD]

This may be more expressive but the problem is then performance takes a hit because if we want to look for all the information from the first table we have to do a bunch of joins.
This means going off to disk a bunch of times and back to memory bleh its slo.

Let's say we have the following table(text view pls):

student-id	|	dept-name	|	instructor-id
------------|---------------|----------------
1			|	Biology		|	10
1			| 	Chemistry	|	20
2			|	Biology		|	10


## Lab Excercise

1. BCNF: the form a table follows if it doesn't have redundancy which comes from functional dependency
2. `order(CustID, CustName, ItemNum, Date)`: no because name depends on id?
	* close: No. we can still create one more table aside with `id name`. we can create `id itemNum date`
