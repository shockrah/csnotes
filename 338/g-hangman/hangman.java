// Name: Alejandro Santillana
import java.util.Random;

import java.util.ArrayList;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class hangman extends Application{
	public static String target_word;
	public static char[] masked_word;

	public static int attempt_count = 1;

	public static TextField input_field;

	public static void reveal_char(char c) {
		// we should go through again and reveal any new characters
		for(int i = 0;i<target_word.length();i++) {
			if(c == target_word.charAt(i)) {
				masked_word[i] = c;
			}
		}
	}

	public static Boolean checkInput(String str) {
		if(str.length() > 1 || str.length() == 0) {
			return false;
		}
		// fallthrough checking proper length inputs
		char c = str.charAt(0);
		if(c < 'A') {
			return false;
		}
		if(c>'Z' && c<'a') {
			return false;
		}
		if(c > 'z') {
			return false;
		}
		// fallthrough for letters of length of 1
		return true;
	}


	@Override
	public void start(Stage stage) {
		// draw the first image
		image = new Image("./img/h1.gif", false);

		Label label = new Label("Word to guess: " + new String(masked_word));

		// seutp the input field
		input_field = new TextField();
		// scene for things to live inside of 
		Scene scene = new Scene(new StackPane(label), 640, 480);
		stage.show();
		stage.setScene(scene);
	}

	public static void main(String[] args) {
		// First we'll read in our file
		ArrayList<String> words = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader("words.txt"));
			String line;
			while((line = reader.readLine())!= null) {
				words.add(line);
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		// pick a random item from our list as the word to guess for
		Random r_idx = new Random();
		int index = r_idx.nextInt(words.size());
		target_word = new String(words.get(index));
		masked_word = new char[target_word.length()];

		// seutp the masked word with underscores
		for(int i = 0;i<target_word.length();i++) {
			masked_word[i] = '_';
		}

		// start the application thing
		launch();
	}
}
