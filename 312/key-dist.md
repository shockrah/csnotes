# Key Distribution

A wants to talk to B but securely(duh).
A trusted third party steps in to provide keys


## Needham-Schoeder

> _used to authenticate users_

Say we have A & B that want to talk.

```
A sends (IDA, IDB) to KDC
KDC sends Enc(SessionKeyA, IDB, Nonce1, Enc(SessionKeyB, SharedKeyA, IDA)) to A
A sends Enc(SessionKeyB, SharedKeyB, IDA) // this bit came in the package from last round
...
...
```

## Denning

Fixes step 3 with timestamps. We also shorten the overall process.
Problem: both parties need to be time synchronized.

NOTE: the Sessionkey below is shared between A and B. THe SharedKeyX is shared between the Key Distribution Center and the user.

1. A sends {IDA, NonceA} B
2. B sends {NonceB, Enc(SharedKeyB, [IDA, NonceA, TimeB])} to KDC 
3. KDC sends {Enc(SharedKeyA, [IDB,, NonceA, SessionKey, TimeB), E(SharedKeyB, [IDA, SessionKey, TimeB]), NonceB} to A
4. A sends {E(SharedKey, [IDA, SessionKey, TimeB]), E(SessionKey, NonceB} to B

## Kerberos

The model itself can be  thought of similar to 
