# Intranets

Topics:

Focusing on what hardware(things really) is used to setup networks in different places.

## Bridges

Used to divide a network thus giving us a form of encapsulation on our network and ergo allow us to delegate some responsibility outwards.


## Ethernet networks

> Hubs

They basically just act as repeaters for Ethernet signals

_With hubs we find that one message gets sent to everybody but only picked up and used by the intended receiver_



# Switches

Associates ports w/ mac address of whatever is plugged into that port.
Reads the source/destination in the Ethernet frame header to create that association.

This is much better because now the whole network doesn't get spammed like with hubs.
Also the collision domain lies between the switch and the host connected now.


# LAN Segment w/ Routers

If we have two subnets next to each other: _routers_ allow comms between the two.

# VLAN

VLANs themselves are similar to subnets but really act as subnets for routers.
