/*
Class: 
	Student
Description: 
	Structure to represent students in the school system
*/
public class Student {
	public int id;
	public String name;
	public int courseID;
	public float floatGrade;

	// helper attributes
	public float average;
	public String courseInfo;
	// using a string to allow for + and - grades
	public String letterGrade;

	public String status;

	public Student(int id, String name, int courseId, float floatGrade, String letterGrade) {
		this.name = name;
		this.id = id;
		this.courseID = courseId;
		this.floatGrade = floatGrade;
		this.letterGrade = letterGrade;
		// default student status
		this.status = "Enrolled";
	}
	public String toString() {
		return "Student number: " + id + "\n" + 
			"Name: " + name + "\n" + 
			"Status: " + status + "\n" + 
			"Courses Enrolled: " + courseInfo + "\n" + 
			"Average: " + average;
	}
}
