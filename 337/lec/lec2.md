# lec2 

## Binary Bits & Bytes 

>  Binary Notation 0b... 

Typically we see `0b` but sometimes like in many x86 assemblers we'll see `...b` to denote some bit string.

Most typically we deal with binary(when we do) in nibbles or 4 _bit_ chunks which then grouped into 2 groups of 4 to build up a byte.
Ex:`0101 1100` is a basic random byte.
For most sane solutions this is essentially the only way we __ever__ deal with binary.


## Two's Complement - aka Negate 

To find the Negation of any bit-string: 

1. Flip all bits in the bit-string
2. Add 1 to the bitstring

### Signedness 

> Why? 

Because this matters for dealing with `signed` and `unsigned` values. _No it doesn't mean positive and negative numbers._
Say we have 4 bytes to mess with. This means we have a range of 0000 to 1111. If we wanted pureley positive numbers in this range we could have 0000 to 1111... or 0 to 15.
If we needed negative represenation however, we have to sacrifice some of our range.
Our new unsigned range is 0-7. We say it's unsigned because the first bit here is 0.
If it were 1 we would have a _signed_ number. 

## Intro to hex

> Hex Notation 0x... 

x86 assemblersi(masm) will typically accept `...h`

More convinient than binary for obvious reasons; namely it doesn't look like spaghetti on the screen.

Our 4-bit range from earlier {0000-1111} now becomes {00-ff}.
More pedantically our new hex range is 0x00 to 0xff.

> Binary mapped 

It happens that 1 nibble makes up 0x00 to 0xFF. 
So for now just get used to converting {0000-1111} to one of it's respective values in hex and evetually it should be second nature.
Then just move on to using hex(like immediately after these lessons).
Even the most obfuscated binary files out there don't resort to architectural obfuscation; until they do.

> Ascii in Hex Dumps 

Kind of a side note but most ascii text is from 0x21 to 0x66ish[citation needed]

## 32 v 64 bit 

For those with a 32 bit background know that these notes deal with 64-bit architecutres mostly. So some quick terminology which might randomly throw you off anyway.

> double-byte/ half-word 

The latter is dumb but soemtimes used so wtever.

> word = 4 bytes

Etc onward with doubles, quads...

