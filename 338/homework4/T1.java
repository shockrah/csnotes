/*
Author: Alejandro Santillana
Date: May 6, 2019
Description: Builds a basic school data tracker
	keeps track of students and what course they are enrolled in as well as what courses
	are offered by the school.
	Course information is also available
	Input is read from file
*/
public class T1 {
	public static void main(String[] args) {
		String dataFile1 = "data1.txt";
		String dataFile2 = "data2.txt";

		// course reference for demo purposes
		Course course1;
		Student student1;

		// first we'll read in some data
		System.out.println("===== Read Data 1 =====");
		School SCD = new School("CSUMB");
		// get all basic info about the SCD
		SCD.readData(dataFile1);

		System.out.println("\n===== School Info 1 =====");
		SCD.schoolInfo();

		// round 2 of adding info to the SCD
		System.out.println("\n===== Read Data 2 =====");
		SCD.readData(dataFile2);

		System.out.println("\n===== School Info 2 =====");
		SCD.schoolInfo();

		// we'll now add some new instructors
		SCD.addInstructor(700, "E. Tao", "tao@csumb.edu", "777-777-1234");
		SCD.addCourse(300, "CST300 – ProSem", 700, "BIT110");
		SCD.addCourse(231, "CST231 – Intro C++", 100, "BIT104");

		// examples of bad courses to add
		System.out.println("\n===== Failed Course Addition =====");
		SCD.addCourse(306, "CST306 – GUI Dev", 250, "BIT120");
		SCD.addCourse(499, "CST499 – iOS Dev", 150, "BIT104");
		
		System.out.println("\n===== Detailed Course Info =====");
		SCD.courseInfo(306);

		// updateing a courses location
		course1 = SCD.getCourse(205);
		course1.updateLocation("Library 104");

		// checking some courses' information
		System.out.println("\n===== Detailed Course Info 2 =====");
		SCD.courseInfo(205);

		System.out.println("\n===== Detailed Course Info 3 =====");
		SCD.courseInfo();
		SCD.deleteCourse(231);
		SCD.deleteCourse(336);
		SCD.deleteCourse(338);

		System.out.println("\n===== Detailed Course Info 4 =====");
		SCD.courseInfo();

		// adding a student (valid this time)
		SCD.addStudent(5555, "Chris Watson", 205, 85.50f, "B");
		System.out.println("\n===== Detailed Course Info 5 =====");
		SCD.courseInfo(205);
		student1 = SCD.getStudentInfo(7777);

		// student info
		System.out.println("\n===== Detailed Student Info =====");
		System.out.println(student1);

		System.out.println("\n===== Detailed Student Info 2 =====");
		System.out.println(SCD.getStudentInfo(7777));
		SCD.graduateStudent(5555);

		System.out.println("\n===== Detailed Course Info 6 =====");
		SCD.courseInfo(205);
		SCD.graduateStudent(5555);

		System.out.println("\n===== Good Job! Bye! =====");
	}
}
