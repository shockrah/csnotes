# Single Source Shortest Path

# Bellman-ford Algorithm

Time: O(VE)

# Floyd-Warshall

Space: O(V^2)

That space is because we're using a matrix to store the paths, which of course is going to take up two dimensions

Main idea: Shortest path between any two nodes in a graph w/ V nodes _will_ go through at most V nodes

Iterative idea: Path's can visit i intermediary node. Does that many any path shorter?

Advantages: we can find negative cycles(Cycle where there is a negative edge)
