# Tries - Prefix Trees(syntax trees in disguise)

We can build grammers from symbols where symbols follow a set of rules to build on each other.
Kinda like linguistic sytax trees but, exactly the same.
_Individual syntaxes are combined to build grammers combine to build phrases, etc. etc._

Instead of symbols we use _prefixes_ as our terminology, to build _words_.
Terminally sequenced symbols are denoted by a _leaf_ flag. 

# Nodes

Nodes in tries are a bit _special_ in that they carry some metadata. 
	* _Leaf flags_: just because a node is set as a leaf, it doesn't mean the node itself is a leaf in the trie

# Deletion

We don't actually remove things for trivial cases.
Instead we turn off the leaf flag in the end target node

if we have /bathe/ and /bathes/ as valid phrases and wanted to remaove /bathe/ from our language
	* All we have to do is set the /e/ to off as a valid leaf.

If instead we wanted to remove /bathes/ instead we would go to /s/ and then set it to off like before.
	* The problem now is that /s/ is hanging and it doesn't have any children so we can remove it entirely
	* If a toggled off node has children it means that it necessarily is part of a valid prefix so it can not be removed
