# Data storage

## Spinning Disks

Cheaper for more storage


## RAID - Redundant Array of Independent Disk

Raid 0: basically cramming multiple drives and treating them as one. Data is striped across the drives but if one fails then you literally lose a chunk of data.

Raid 1: data is mirrored across the drives so it's completely redundant so if one fails the other is still alive. It's not a backup however since file updates will affect all the drives.

Raid 5: parity. Combining multiple drives allows us to establish the parity of the data on other drives to recover that data if it goes missing.(min 3 drives)

Raid 6: same in principle as raid 5 but this time we have an extra drive for just parity.

Raid 10: 0 and 1 combined to have a set of drives in raid 0 and putting those together in raid 1 with another equally sized set of drives.

## Network Attached Storage - NAS

Basically space stored on the local network.

## Storage Attached Network - SAN

Applicable when we virtualise whole os's for users, we use a storage device attached to the network to use different operating systems


# Managing Storage

Outsourcing the storage for users to services like Onedrive because it becomes their problem and not ours.

# Storage as a Service

Ensure that the OS gets its own space/partition on a drive and give the user their own partition to ruin. That way the OS(windows) will just fill its partition into another dimension.


# Backup

Other people's data is in your hands so make sure that you backup data in some way.
Some external services can be nice if you find that you constantly need to get to your backups.
Tape records are good for archival purposes; keep in mind that they are slow as hell.


