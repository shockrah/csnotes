# lec12

Networkk Layer

Here instead of datagrams, or segments, we are referring to chunks of data as _datagrams_.
We concern ourselves with _host to host communication_.
There are two major function to worry about:

* forwarding
	* getting from an input to an output
* routing
	* concerned with 

## Virtual Circuits

_THIS SECTION IS BAREBONES_ 

Datagram Service: network provides network-layer _connectionless_ service.

Virtual Circuit: network provides network-layer _connection_ service.

### Setup a connection

Virtual Connections: before two end hosts start sending data to each other both must determine over which route they will communicate with.
This means that we have to get routers between hosts involved with this initial setup.

When the routers get involved they put an incoming vc number on an incoming bus, and an outgoing vc number on an outgoing bus.
These entries are stored in the routers _forwarding table_.

## VC Implementation

1. Path from source to destination
2. VC numbers one number for each link
3. entries in forwarding table

### Forwarding Table

__pls clarify section__

Each router has a forwarding table which allows for entries regarding data about it's incoming/outgoing buses.

Router has has incoming/outgoing interface on these lines we see that datagram has a vc# when its incoming to the router.
Upon exit we know that each vc# from the incoming interface corresponds to a vc# on some outgoing interface.

## IP Addresses & Datagram forwarding tables

> What is an ip address?

Think of an adress of a variable in memory.
Instead of a variable we have an end host.
Instead of an address memory we have an address in some network.

Usually we'll write a destination address in the header of a datagram so that we know where the data is meant to go.
