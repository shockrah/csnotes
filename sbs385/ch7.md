California State University Monterey Bay

SBS 385 *Environmental History of California*

***California: A History* (Starr 2005)**

**Chapter 7: Great Expectations: Creating the Infrastructure of a
Mega-State**

**In the second forty years as a state, the public works infrastructure
of California was established**. The **dams, aqueducts, reservoirs,
power plants, industrial sites, bridges, roadways, public buildings**,
and stadiums served a growing population. They also foretold and
empowered the mega-state to come.

**It began with water**, the sine qua non of any civilization. Two
thirds of the annual precipitation falls in the northern third of the
state, much of Southern California is desert terrain, despite two great
rivers, the Sacramento and San Joaquin, the Great Central Valley is
itself a semiarid steppe, with soil baked by the sun to such hardness
that it frequently had to be broken with dynamite. For California to
become inhabitable and productive in its entirety would require a
statewide water system of heroic magnitude.

In 1878 the legislature passed the Drainage Act and appropriated funds
for irrigation, drainage, and navigation studies. These studies put
California into the historical context of irrigated civilizations of
ancient and modern times.

The **Wright Act of 1887** empowered local communities to form
**irrigation districts** to divert river water to dry lands for
irrigation and/or flood control, thus establishing the legal and
political framework to bring water to previously arid land, and, in
short order, **transform the Central Valley and portions of Southern
California into an agricultural empire**.

Developers were really **selling not so much land as water**. The
Imperial Valley was promoted as the Egyptian delta of the United States
with the Colorado River serving as its Nile. Promoters advanced **a
biblical scenario** with Americans being called by the Lord to a life of
missionary improvement. Irrigation, however, was a reorganization of
nature, and all such reorderings have their risks.

The **technology developed** in the Gold Rush for moving water across
land led to the technology of irrigation, which would enlarge and
stabilize the metropolitan infrastructure of San Francisco and Los
Angeles. By 1900, 40% of the 1.5 million population lived either in the
Bay Area or Greater Los Angeles, and each city knew it would need more
water to serve its present population and support desired growth. Each
city established an administrative board to develop water plans and
programs, and each city pushed a major water project to a successful
conclusion by tapping, in each case, a river – the **Owens for Los
Angeles**, the **Tuolumne for San Francisco** – and bringing its water
to the city through a system of dams, reservoirs, and aqueducts that
took years to construct.

The **water and hydroelectricity** thus obtained enabled each city to
serve as many as 4 million residents, and in each instance, the water
system involved almost equally monumental damage to the environment. In
the case of San Francisco, the loss of the magnificent Hetch Hetchy
Valley near Yosemite when the Tuolumne River was dammed and the valley
was filled to create a reservoir, and in the case of Los Angeles, the
desiccation and devastation of the once-fertile Owens Valley when the
Owens River was siphoned off to Los Angeles. Each project, moreover, was
plagued by claims of deception, double-dealing, and conflict of interest
that became the subject of many histories, novels, and films, including
the Oscar-winning *Chinatown*, in the decades ahead.

It was easy to make a living in this **booming economy**. The
construction of the Los Angeles Aqueduct kept thousands employed,
agriculture and related activities (packing, shipping, canning, food
processing), transportation and shipping, the forth busiest port in the
nation, home building constant across three decades supported building
trades (carpenters, plumbers, painters, landscapers), the oil industry,
automobiles, the hotel and tourist industry, aviation and motion
pictures, fishing industry, military bases, and numerous colleges and
universities.

The **Great Depression** of the 1930s witnessed the continuing creation
of a statewide infrastructure as the state and federal governments
sponsored ambitious programs of **public works** that, in effect,
completed California: Hoover Dam (1935); **Central Valley Project** and
associated dams, reservoirs, and aqueducts; **defense**
**manufacturing**; state and county **roadways**; San Francisco-Oakland
**Bay Bridge** (1936); and **Golden Gate Bridge** (1937).
