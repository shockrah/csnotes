# lec2

> Packets

When we send data(messages) we'll typically split the data into chunks or __packets__

## Network 

> packet switching

Hosts break app-layer msgs into packets. These packets are moved along the same path like a train from router to router. Each of these moves from router to router we call __hops__.

## Store and Forward 

We first store the entirety of the packet before sending it off. Usually we keep this in some kind of buffer where we can read what kind of packet we are dealing with through some simple parsing. This is done so that we know how large the packet is going to be since the header of that packet will tell us crucial information about it and others like it.

## Packet Switching: Queueing delay, loss 

Since the routers store the packets in memory before sending them off, they store them in some queue which can sometimes fill up. For that reason if the buffer fills the router drops the packets and keeps going.

Packet loss at the network layer is expected which is why we usually have packet loss recovery at each end. 

## Two key network-core functions 

Packet headers usually tell us where the packet is trying to go and if where the other packets which stitch to them are supposed to go as well. \
This information is usually kept in the header of the packet file. 

```
HEADER:
DEST: 10101001
DATA: 1001000 ... 00011110
REL: <>
```
It is important that we keep some relativity in the stream of packets.

### Circuit Switching

FDM v TDM [Frequency Division Multiplexing] [Time Divisiono Multiple]

> FDM 

Multiple frequencies which remain static over time we can use for different connections.

> TDM 

We use the same channel but this time we use slices of time allocated to different connections.

## Packet switching v Circuit Switching 

_Packet switching let's more users on a network at once over circuit switching_
The reason boils down to user behavior on a network.

Packet switching advantages: 
* great for burst data 
* easy to setup typically

Disadvantages:

* Packet delay and loss 
* congestion does happen
* you need to implement some kind of protocol for data loss recovery


