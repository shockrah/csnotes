# lec21

## Strong & Weak Entity sets

Strong: has a primary key in the set

Weak: opposite of strong

## Diagram things 

_pretty formatting on diagrams means stuff_ 

* Arrows

* solid lines
	* many 
	* Contributer [solid] contribution [solid] candidate

* dotted lines
	* only one 

Let's say we have solid/arrow

```
student{id/name} --- [advisor] --> instructor{id/name}
```
Students can have at most 1 advisor \
Instructor's can advise multiple students

If we want to have some kind of advisor table we can identify each relationship with _just_ the student-id.
We can do this because each student will only every 0,1 instructor's in an adivsory relationship

## Composite Structures

Logically compositing makes sense but SQL does not like much aside from primitives so we can't really do that 

Say then we have: 
```
contributor:
id
name
address		<-- f to pay respects
	city
	state
	zip
```

We just drop the address part if we wanted to convert this to something in sql.
Reasoning here is case by case likewise need-to-know basis: ergo stick to da plan.

## Normalization

Process used to imporve schemas

Basically a method of setting up rules to stop random bs from happennig.
Also we typically remove redundancy from our schemas through this process 
