#!/usr/bin/python3
import random   # just for some basic population methods

class Node:
    def __init__(self,data):
        self.data   = data
        self.children = []

    def __str__(self):
        return self.data

    def __repr__(self):
        return f'{self.__str()}:{self.children}'

def new_node(data):
    return Node(data)

def add_node(node, lr, val):
    node.children.append()
    return None

def lvr(root):
    """
    Inorder traversal
    """
    # left sequence
    if root.left is not None:
        lvr(root.left)

    # visit sequence
    print(root.data)

    # right sequence
    if root.right is not None:
        lvr(root.right)

    return 
