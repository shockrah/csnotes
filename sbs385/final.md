---
title: Final Exam
author: Alejandro Santillana
subtitle: CST 385 - Spring 2019
documentclass: scrartcl
date: May 10, 2019
geometry: margin=1in
---
\pagenumbering{gobble}


Part of the reason why cities like Los Angeles or San Fransisco have become as large as they are is because 19^th^ and 20^th^ century settlers had initially stayed in those areas because of several factors: lumber, and accessibility for travel.
Considering the technology available to settlers, it made sense that they would settle in places easily accessible by sea as it was a much more efficient way to travel.
This meant that finding natural harbors along the coastline could suggest a potential place to settle.
In the 19^th^ century especially this was even more true considering that spanish settlers came primarily from the south by sea first, before permanently settling by land.

During the process of mining gold miners would dig up soil to then sift through either with machines or with pans.
The unwanted material, the dirt, rocks and other such things were usually just thrown back into the river.
Because of this the now unsettled debris would make its way down rivers, polluting the local fish in that area, as well as disturbing things downstream.
Terrestrially, the torn up river banks were only one part of the problem, as the miners still had to make camps and small towns where they could live.
Therefore the surrounding forestry would be cut for materials for further mining.
As the surface gold was exhausted miners turned to hydraulic mining which caused entire hillsides to be geographically reformed, and the debris would get washed away downstream.

Chapter 5 discusses the later half of the 18^th^ century and specifically examines how bot people and money was moving throughout the state at the time.
While the gold rush had brought a large bounty for those who could claim it the land which the state offered was still a massive portion of the available wealth in California which was still largely debated.
Part of the issue was that landowners across California were really in largely distinct areas, from urban places like the San Fransisco Bay Area and Southern California which was barely settled.
For this reason there was tension between legislators in Washington D.C. trying to make all of California a single state, and landowner which asked for seperation due to the various regions' distinctions.
Even as issues of land ownership were discussed industries continued to give way to progress in one form or another. 
It is because of the gold rush that so many people moved into the state at once, creating a large demand for agriculture, giving farmers and ranchers alike a reason to stay.

Chapter 6 explains how Over time Californians began to find some sense of identity as art in California found its place.
This becomes especially evident when there was much for artists, especially landscape painters to discuss and present.
And just as before with the gold rush there was in the 1880's the completion of the transcontinental railroad, which allowed more people to come into the state.
With travel to a place of new opportunity quicker than before more people could come to the state should they be looking for a potential new opportunity.
What this meant for the landscape of California was that there would be development spreading across the state from centers like San Fransisco or Los Angeles.
The largest source of this development across the state was in fact the railroad as well, because it had to span across so much land in California.

As the railroad grew so did the need for water across the state.
For that reason 1870's and 1880's saw a significant push to control the water as evidenced by legislation such as the _Drainage Act of 1878_, which provided funds for irrigation research.
It's with these funds that allowed work to be done provide water to areas such as San Fransisco or Los Angeles where the populations made a significant portion of the state's overall population.
The effect of this diversion of water meant that the Hetch Hetchy Valley and Owen's Valley were drained completely altering the local environments in a seriously damaging.
Because the population kept growing however a increasing demand for water pushed the creation of more dams and reservoirs throughout California.
All of this of came at the cost of changing significant portions of California's ecology and geology.
The California State Water Plan shows an even larger scale change than before.
This time however, instead of just diverting the flow of rivers and streams, the plan also included storing water so it can used elsewhere.
This furthers the issue of tampering with the natural water cycle which creates a net loss for wild rivers as they will often decrease in size.

Chapter 8 of _Green Versus Gold_ describes the change for farmer families which caused them grow from effectivly small entities to large busisnesses.
As California's population grew so did the need for food so of course small farms grew along with the population.
Slowly becoming more corporate in some cases as the demand of the ever growing communities also grew.

To take an example of a protected resource the National Redwood forests protect lumber as a resource.
More than just lumber of course is protected in these parks however, as there are full ecosystems which are protect by association of being part of the parks.
Apart from the usual natural resources found in parks there are also culturally significant sites such as Fremont's Peak in Monterey County.
While there isn't raw material resources to use it is left protected because it serves as a cultural reminder of a significant point in California's history.
In both of these cases one can see a representation of California's bounty, be it in the form of lumber in the Redwood forests or the potential for opportunity represented by Fremont's peak.

The 1930's and 40's for California saw large changes to the infrastructure of the state's water system.
With new dams being built to attempt to provide necessary resources to the ever expanding population it was in some senses a time of growth.
While writer's and artists may have expressed feelings of closeness with the landscapes and natural beauty California had, still there was not nearly as much support for such feelings as later in the 60's and 70's.
It is in the later 60's and 70's that more support for California's environment emerges as we learn more about the human effect on the surrounding nature.
Humans having an effect becomes completely apparent as one starts to look back on history keeping in mind environmental state.
A more educated population then would have have more likely to usher in an event like Earth Day, where perhaps such an event could not have existed prior.

Given the history of people and California, the place it would seem that the title, _The Death and Life of Monterey Bay_ would allude to how the state had been mistreated.
An example of the _Life of Monterey Bay_ after its apparent death is the otter population which many believed to be extinct.
A treaty in 1911 prevented otter pelts from being traded which meant the tiny otter population could slowly grow to an appropriate size.
Algae populations were also rising due to, "The removal of sharks by the world's fisheries ... let ray populations bloom in some estuaries, and the rays have eaten the scallops there... the resulting bloom of algae smothers the corals" (Palumbi, 150).
By removing humans from meddling with the natural order of things in Monterey Bay this blooming algae population slowly decreased as well because the fish in the area were able to recuperate their population.
It is because of the concious decision that people made, to protect the local wildlife, which allowed things like the otter population to recover, or the algae blooms to be more controlled.

Class activities help to achieve the goals which CSUMB strives for as often they allow students to participate and practice that which is taught to them.
Whether the activity is a more "_hands-on_" activity or an informal discussion just allowing the students to try new things allows the students to learn to be more autonomous.
As for suggestions class discussions fueled by contemporary events relevant to the course material always seemed to be the most interesting.

My hometown is Aliso Viejo, a small suburban town in southern California where the climate is typically much dryer than Monterey Bay.
Though the town is mostly suburban there is a healthy population of wildlife which has been protected for some time as part of Orange County's environmental conservation efforts.
Because of these efforts there is both wildlife and natural streams that cut between some of the hilly landscape.
Relatively speaking Aliso Viejo is among the more prosperous areas in California from a economic point of view.
Because of the economic state and culture of Aliso Viejo it means that ongoing conservation efforts are maintained in their current state.

Overall this course has shown me personally just how deep the history of California goes and has shown parts of the state which I hadn't known existed before.
Coming from my hometown and spending so much time in larger cities clouded my view the rest of the state and this course has help to give me more clear image of the state at large.

To close it is paramount that we look to through the environmental history of California to find what patterns have existed in order to avoid previous negative trends when possible.
The impact we as a society have on the environment around us is often either irreversible or nearly so to limit scenarios of attempting to reverse poor decisions we should use history as a means to avoid such scenarios.
Primarily we should measure our actions unlike before when we built dams, reservoirs, or even cut down whole forests so that we can avoid unforeseen and irreversible damages.
