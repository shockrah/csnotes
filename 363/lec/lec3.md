# lec3

## Relational Algebra and its relation to SQL

### SELECT
Used to select columns from some table.

> Relational symbol: pi

### Projection
Picks out the rows of a (set of) table(s)

> Relational symbol: sigma

### Union
Adds the rows of two tables into some new table. 
Removes duplicates from resultant table as well.

> Relational symbol: U(looks like a U but w i d e) 

### Relational Algebra on paper

Even though conceptually this maps straight to SQL there is a handwritten way to express this on paper. The following is a _plain_ english way of reading these pape operations.

> /select_(concatenation of fields)[target table]

> /project_(_list of fields_)[target table]

> {resultant table} /union {resultant table}

## Cartesian Product

We take the _crossproduct_ of two relations(tables)

Take the __N'th__ row of the first and combine linearly with the rows from the second table.
The result should be a new table with R1_(rowcount) * R2_(rowcount).
This result isn't something that we would particularly care about since there is so much data which is now mixed throughout.

## SQL 

Declarative language which mainly queries databases, and deals with schemas. 

> Data-Definition language (DDL)
* Create/modify/delete schemas
* define integrity contraints 
* define views
* drop tables

> Data Manipulation Language (DML)
* Queries and whatnot

### Define a relation Schema

```
create table tableName (
	fieldName	type(argv...),
	...
	fieldName1	type(length) not null,
	primary_key(fieldName,[...]),
	foreign_key(fieldName,[...]) references otherTable

);
```
#### Contraints

`not null`: Requires that the field not be null when data is being inserted.

`primary key`: Can be used inline to show that some field is a primary key on its own.

### Field Types

`varchar(length)`: _variable_ length string of characters. If we have `6` as a maxiumum then we may only end reserving `4` bytes(words) of space somewhere.

`char(length)`: _Fixed_ length string of characters. If we have `5` then we must have a string length of `5`.

`numeric(p,d)`: Fixed-point number of `p` digits with `d` digits to the right of the decimal.

