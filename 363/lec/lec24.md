# lec24

## NoSQL

Why NoSQL is a thing

1. Scaling

Scaling demands more computing power over time.
Relational databases require more vertical scaling where machines must be upgraded constantly.

Alternatively horizontal scaling allows for clustering computing style upgrades where computational power can be cheaper for upgrades maintaining support for large userbase.

2. Data Migration

If you change a schema then you have to painfully move everything over with the new schema.
This means downtime in some cases, no matter how small the change.

3. OOP

`Rows != Objects`: the key distinction here is that objects can contain other objects while SQL rows can not.

4. Open Source

Many of the more popular NoSQL database systems happen to be open source and have permissive licensing.

Getting started can suck though because you have sometimes have to implement certain features with something like _MySQL_ already have.
