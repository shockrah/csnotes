# lec4

Get a basic conceptual understanding of the application layer on the network stack.

## Application Layer Intro

Things we don't care about in this layer:
* Routing
* Network buffer(queue) overflows

These things should be handled by the network layers beneath this one.

### What the App-layer defines

* types of messages

Requests and responses.

* Message syntax

* Message Semantics

* Open protocols

* Rules

## Client Server Architecture

> Server

Hoster of content, usually with a static ip address

> Client 

Requesters of content, ip addresses here aren't necessarily dynamic however if you host a server of some kind, __never__ assume a client has a consistent ip. 
Also clean any requests from users to avoid random catastrophe.

## Peer to Peer

Here there is no concept of a host that is always on and has a static ip. Instead we have clients communicating with each other directly.

## Sockets

> processes which allows applications to send/receive data.

> `man socket` in the terminal on any *nix based system(_maybe not mac_)

Each socket will use some port for which to communicate through. 

## TCP & UDP Protocols

> TCP

Provides flow and congestion control.

Drawback is that we have someover head for the safety.

> UDP

Does _not_ provide flow or congestion control.

Advantage is that we don't have to concern ourselve with any packet safety, like accounting for dropped packets.

Streaming services would be a good use case since it's more acceptable for a _milisecond_(colloquially speaking) of data to be lost since the user probably won't notice anyway.

* More and more networking level improvements and hardware improvements however allow us to use TCP with its overhead without worry.

## HTTP Protocol

Uses the client/server architecture assumptions.

Clients make `GET` requests and Servers usually send `OK` packets.

> KeepAlive

Modern(ish) concept that a connection opened with a server/client should remain open for some time; that way we don't have to reopen that connection for every single request a single user makes.
_Yes this save us on overhead, and yes its worth it_.

> Stateless

Maintains no information about past client requests.
Cookies add the concept of statefulness.

## HTTP Structures

Now we'll go over some of the various structures which HTTP uses.
