# lec20

_more on mapping cardinalities_

## Pariticipation Constraints

These are _perscriptive_ constraints.

* total 

If _all_ the entities in a set participate we say that the participation is total for that set.

* partial

If even one entity in the set is not participating then the pariticpation is _partial_ for that set.

## Entity Keys

If we have a table for a relationship, we can identify all the relationships if we can uniquely identify enties from either given set.
Essentially if we can identify both(all) pariticipants in a given relationship table we can find any relationship in our relation-set.

## Weak Entity Sets

Any set where we can not uniquely all entities in that set.
Let's say we have a tournament.

We'll have players, with some _name_ and _jersey number_.
We also have teams with a _team-name_ and likely some _team-id_.

This means our players entity set is a weak set, but, because _all_ players participate in a team by definition of what a player is.
Furthermore we may use the relationship between teams and players to idetify players.

