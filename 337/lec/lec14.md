# lec14

Just like any other binary, those written for MIPS must follow a specific structure for any level of compatibility.
In the case of MIPS, binaries have a few _sections_ which allow us to cleanly organize our files.

_Note that for the rest of this course I'll be discussing the [ELF file format](https://en.wikipedia.org/wiki/Executable_and\_Linkable\_Format), although most of this information is conceptually the same across most other [binary file formats](https://en.wikipedia.org/wiki/Binary_file).

## From Disk to Memory 

### Binaries on disk will have the following structure

* Header
 * Think of this like the metadata for the whole file
 * Keep in mind however that every section also has its own respective header that let's the O.S. loader know what is going on.
* Data section
 * Here you'll find any compile time constants and compile time variables held in this section
* Text Section
 * This is where the _code_ actually live on disk. Any functions, loops, etc that gets compiled from something like C++ or C will be written to this part of the file.

### Binaries in memory


Once the binary is passed through the system loader the operating system will be able to determine that we may need a stack for the newly loaded code.
I say "may need" because not all binaries require its own stack.
A shared object file might not need its own stack because it will only be used as kind of _extension_ by some executable binary.

At this point any previous structure from disk is preserved as one might expect.
Once the file is loaded into memory however, the system loader must then also allocate some space of memory for our heap and stack; finally our program is in memory and we can begin execution(right?).

## DataTypes 

Truth be told there are no _types_ in mips or really any assembly language, instead we determine _types_ by their size in bytes.
In the case of MIPS we have the following types 

| Type		|	Size	|
| ---		|	---		|
| byte		|	1		|
| half-word	|	2		|
| word		|	4		|
| dword		|	8		|

Likewise we also have _registers_, which are similar to global
## Actually writing mips now

Just like any other sensible architecture we have the ability in MIPS to use system calls to let us do things like writing to stdout or reading from stdin.
If you're writing mips code in mars there's a useful list of syscalls [here]().

For now let's translate some code to disect the different parts of MIPS.

```
int main(void) {
	write("hello", 5, 1);
	return 0;
}
```

If you compile this you'll have 1 compile time constant which contains `hello` but in hex followed by 1 byte of `0x00`.
We also have some code, in this case we want to write something to stdout, then we want to exit the program safely.

From the top we have a need for some data: 

```
.data	
.text
```
