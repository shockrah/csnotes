# lec9


_Continuation of RDT_ + lec8

## Pipelining

Instead of sending just one packet at a time we can send N packets across the network simultaneously.
We have two choices to deal with errors in this case however, `go-back-N` or `selective repeat`.

### Go-Back-N

THings we'll need to deal with:

* timeout
* recevier acknowledgement

#### Sender 

Sends N packets at a time and only re-sends according to highest value `ack` the receiver responds with.(_the next section explains this better_)

#### REceiver

We will respond with the _highest_ successful sequence number.
Say we were supposed to get 1 2 3 4 but 3 failed.
This is when we send ack1 ack2 but not `ack3 or 4`. 
This tells the sender to resend everything from 3 onwards.


### Selective Repeat

Receiver gets `1 2 3 4` but `3` fails.
This time we send `ack1 ack2 nack3 ack4` and `3` is resent.
_This will take less bandwidth and still correct issues which can randomly happen._

If we imagine a window moving along a row of packet slots we only move that window along the row as the lowest(sequenced) packet is filled.
If a packet isn't `ack`d we wait for a timeout then we resend to the receiver.

Example: Say we send `1 2 3 4 5` but `3` is lost.

Our window would move from `1-5` to `2-7`, wait for timeout then resend, wait for `ack` then move forward as it receives `3`.
A useful link for visualizing selective repeat can found here: 



