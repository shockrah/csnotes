'''
We'll create some system where we can add things to a list of buckets 
'''

def hashData(data:int, lst:list) -> None:
    # insert data into a list of buckets
    key = data % len(lst)
    if type(lst[key]) == int:
        # setup the new list things
        tmp = [].append(lst[key])
        tmp.append(data)
        lst[key] = tmp
        return

    # here we assume there is collision so we just append things onto our list
    lst[key].append(data)
    
if __name__ == '__main__':
    pass
