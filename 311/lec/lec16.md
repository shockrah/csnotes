# lec16

Now we'll look at an introduction to how routing (sort of) works, with a few constraints.

## Constraints for Routing

We know that no router can have a link to every other router in the world so we know that most data will have to hop over a few routers at least to get pretty much anywhere(welcome to heuristics).
To add onto this constraint, each router will only have knowledge of the routers directly around it.
This means we have a small, easy to update catalog of nearby routers, which hopefully small enough where searching it doesn't take too long.

Further more each router must have some kind of "_length_" concept to determine how "_far away_" nearby routers are.
This could be a ping time to send data to those nodes, or maybe a physical distance.
Whatever the case may be, we have a _length_ or _distance_ to the surrounding nodes.

In essence, these are the only constraints we have to build a routing algorithm.
We know that we have tools like Dijkstra's algorithm but the problem is that with a huge network, say the size of the whole internet we will waste countless amounts of time.
For this reason we have to come up with some kind of _good enough, nice one bro_ algorithm.

## Routing Algorithm Goals

1. Finish Fast
	* graph traversal is cool and all but people have things to do. So we want to reach the end as fast as possible

2. Easy Execution
	* Individual nodes might be comprised of old hardware, slow and sluggish, so give them something easy to compute
