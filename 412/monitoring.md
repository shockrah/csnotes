# Service Monitoring

Several types of monitoring: 

## Building Monitoring System

If your monitoring a lot of _stuff_ know that you will could have lots of stuff going across the network(in some cases).
This is why a good monitoring system shouldn't get in the way of _business traffic_ as the purpose of the network is to service its users.
Ultimately however to ensure that things are running smoothly some monitoring must be put in place which means we must use valuable resources to make sure things are behaving correctly.

How this is done is generally up to implementation but there a few rules of thumb:

* Don't query everything on the network all at once as you could easily clog the network and drop all kinds of data in the process

If you must query _all the things_ then try to stagger the query to a few machines at a time, either by scheduling the checks or doing them randomly to avoid them all at once.

* Limit what kind of data you query for

The reasoning is the same as before, try to not use too many resources at the same time lest you risk dropping both business and query data

* Avoid peak times

All networks have certain hours of the day where usage is at its maximum for the size of that userbase. If you're dealing with a rather large network then definitely avoid doing heavy monitoring tasks during these times.


Know that we have to _get data, store data, and ultimately view data_.

## Historically

SNMP: Usually you will get back a ton of data 
