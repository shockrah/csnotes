California State University Monterey Bay

SBS 385 *Environmental History of California*

**California: A History* (Starr 2005)**

**Chapter 6: The Higher Provincialism: American Life in an Emergent
Region**

In 1908, Josiah Royce (born in Grass Valley, CA), the Harvard
philosopher extolled regional life as something profoundly serving the
human need for community. Americans needed such a personalized
connection more then ever, now that the **United States was becoming an
international empire**. Americans could discover what it meant to be an
American when they discovered their American identity in a localized
context. Royce’s favorite province for analysis was California,
specifically its topography and climate. **California promoted
simultaneously an independence of mind, individualism, and open
simplicity of manner**. California was a prism through which the larger
American identity, for better or worse, could be glimpsed. For instance,
Gold Rush communities were largely male, by turns good-humored or
violent.

Samuel Clemens, working in San Francisco as a newspaper reporter, would
reinvent himself as Mark Twain – canny, observant as to social types and
distinctions, writing a mixed insider/outsider point of view. The
success of his books (*The Celebrated Jumping From of Calaveras County
and Other Sketches* in 1876 and *Roughing It* in 1872) turned him into a
national figure.

Clarence King’s Mountaineering in the Sierra Nevada (1872) represents a
high point in the frontier genre of geological description and the
mountaineering memoir. The writings of King and other geologists of the
California Geological Survey established a record of accurate and
well-written scientific fact, focusing on the history of the Sierra
Nevada and its creation through catastrophe and its storage of geologic
time, evoking the seas, convulsions, lava, and glaciers that created the
Sierra in eons past.

William Randolph Hearst, scion of mining millionaire George Hearst, a
U.S. senator, was editor and publisher of the San Francisco Examiner. He
pioneered journalistic techniques – featured writers, columns, crusading
editorials backed by vivid cartoons, coverage of society, the sporting
world – that he would soon take national.

California supported art from the frontier days onward, and by the 1870s
had firmly established itself as a center for landscape painting. There
was plenty to paint in California, with an emphasis on such signature
places as Mount Shasta, Mount Tamalpais, the Yosemite Valley, Clear
Lake, the Napa Valley, and the oak-dotted hills of the East Bay.
Likewise did photography flourish and early experiments in high
technology would bring into being Silicon Valley and the world it
revolutionized.

In the 1870s, adobe gave way to brick and wood; candles and kerosene
were replaced by gaslight; streets were paved and tracks laid for
horsedrawn streetcars; police and fire departments were organized; a
lending library was established; a city hall, county hospital, opera
house, and theater opened, as **Los Angeles made the transition** from
Mexican to American city.

The opening of the transcontinental railroad route into **Southern
California** precipitated the brief but transforming “Boom of the
Eighties” that finalized the Americanization of Southern California.
**Middle and upper middle class migration for reasons of health,
tourism, winter sojourn, or permanent residence**. The perception of
Southern California as Spanish Colonial daydream helped establish an
expanded metaphor of Mediterraneanism in terms of climate and terrain
and many **parallels to Mediterranean Europe** with comparisons to
Spain, Italy, and Greece. Such an interplay of metaphors helped Southern
California develop their built environment in a manner akin to a stage
set.

As the 20^th^ century dawned, the **population** of California stood at
**1.5 million**, quite a small figure for such a vast state. Nearly half
this population lived in the San Francisco Bay Area, compared with
18,000 people living in San Diego. The railroad constituted the
predominant public works infrastructure of California in the 19^th^
century. The **railroad linked the state, shipped the freight, owned and
developed the land, founded the cities of the interior, and controlled
the political machines of San Francisco and Los Angeles**. The railroad
was the primary fact and symbol of **industrialism**, hence the
commanding icon of **modern** **life**.

Already, sentiment was building for a better governance of this emergent
society. California continued to require a makeover of its public
culture, and reforming its architecture, town planning, business
culture, and politics. The decade of the 1880s was of a **generation of
pre-Progressive and Progressive reformers** who set about the business
of trying to make California worthy of its geographical grandeur.
University students were encouraged to practice high thinking and the
strenuous life, to pursue high-minded, evolution-friendly theism whose
matrix and primary symbol was California as natural place. The Golden
Gate Kindergarten Association incorporated in 1884 in San Francisco
became the model for free public kindergartens in the U.S. and abroad.

At 5:12am on Wednesday April 18, 1906 the Pacific and North American
tectonic plates suddenly sprang from nine to twenty-one feet past each
other along the 290 miles of the San Andreas fault. The **Great
Earthquake and Fire had ended the second phase of California’s
development**, its High Provincial years of regional achievement and
contentment. Ahead lay the challenge and task for the **next era**: the
creation of an **infrastructure** that would make possible a
**mega-state**.
