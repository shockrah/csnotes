/* This object will handle the user input forms for the application 
 * to clean up the front app.java portion
 * */
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
public class Input {
	static public Scene studentInputScene;
	static public Scene courseInputScene;
	static public Scene instructorInputScene;

	// NOTE: we are assuming that input isn't busted for these things as they have no protection whatso ever built in
	static private void submitStudent(String id, String name, String courseID, String grade) {
		int _id = Integer.parseInt(id);
		int _courseID = Integer.parseInt(courseID);
		float _grade = Float.parseFloat(grade);
		
		// right now we don't care about the letter grade so we'll just in put in whatever
		App.school_.addStudent(_id, name, _courseID, _grade, "Z");
	}
	static public void initStudentInput(Scene _return, Stage _stage) {
		// each student takes: INT STRING INT DOUBLE
		GridPane grid = new GridPane();

		// first we'll add all the fields necessary
		Label idLabel = new Label("ID");
		TextField idField = new TextField();
		grid.add(idLabel, 1, 0, 1,1);
		grid.add(idField, 2, 0, 1,1);

		Label nmLabel = new Label("Name: ");
		TextField nmField = new TextField();
		grid.add(nmLabel, 1, 1, 1, 1);
		grid.add(nmField, 2, 1, 1, 1);

		Label cLabel = new Label("Course: ");
		TextField cField = new TextField();
		grid.add(cLabel, 1,2,1,1);
		grid.add(cField, 2,2,1,1);

		Label gLabel = new Label("Grade(Float): ");
		TextField gField = new TextField();
		grid.add(gLabel, 1,3,1,1);
		grid.add(gField, 2,3,1,1);

		// finally the buttons
		Button retButton = new Button("Return");
		Button submit = new Button("Submit");
		grid.add(retButton, 1,4,1,1);
		grid.add(submit, 2,4,1,1);

		retButton.setOnAction(e->_stage.setScene(_return));
		//submit.setOnAction(e->Input.Submit
		Scene ret = new Scene(grid, 1280, 720);
		studentInputScene = ret;
	}

	public static void courseDeletion(String id) {
		if(id.length() == 0 ) {
			return;
		}
		// fallthrough to convert and delete the thing from the school system
		App.school_.deleteCourse(Integer.parseInt(id));
	}
	static private void submitCourse(String id, String title, String instID, String loc) {
		int _id = Integer.parseInt(id);
		int _instID = Integer.parseInt(instID);
		App.school_.addCourse(_id, title, _instID, loc);
	}
	static public void initCourseInput(Scene _return, Stage _stage) {
		GridPane grid = new GridPane();

		// first we'll add all the fields necessary
		Label idLabel = new Label("ID");
		TextField idField = new TextField();
		grid.add(idLabel, 1, 0, 1,1);
		grid.add(idField, 2, 0, 1,1);

		Label nmLabel = new Label("Title: ");
		TextField nmField = new TextField();
		grid.add(nmLabel, 1, 1, 1, 1);
		grid.add(nmField, 2, 1, 1, 1);

		Label eLabel = new Label("Instructor ID: ");
		TextField eField = new TextField();
		grid.add(eLabel, 1,2,1,1);
		grid.add(eField, 2,2,1,1);

		Label pLabel = new Label("Location: ");
		TextField pField = new TextField();
		grid.add(pLabel, 1,3,1,1);
		grid.add(pField, 2,3,1,1);

		Button retButton = new Button("Return");
		Button submit = new Button("Submit");
		grid.add(retButton, 1,4,1,1);
		grid.add(submit, 2,4,1,1);

		retButton.setOnAction(e->_stage.setScene(_return));
		// Finally we'll deal with submitting of the data
		submit.setOnAction(e-> submitCourse(
						idField.getText(), 
						nmField.getText(),
						eField.getText(),
						pField.getText()
						));

		Scene ret = new Scene(grid, 1280, 720);
		courseInputScene = ret;

	}
	static public void setInstructor(String instructorID, String courseID) {
		int _i = Integer.parseInt(instructorID);
		int _c = Integer.parseInt(courseID);

		// now we can assign the instructor to the course
		App.school_.assign(_i, _c);
	}
	static private void submitInstructor(String id, String name, String email, String phone) {
		int _id = Integer.parseInt(id);
		App.school_.addInstructor(_id, name, email, phone);
	}
	static public void initInstructorInput(Scene _return, Stage _stage) {
		GridPane grid = new GridPane();

		// first we'll add all the fields necessary
		Label idLabel = new Label("ID");
		TextField idField = new TextField();
		grid.add(idLabel, 1, 0, 1,1);
		grid.add(idField, 2, 0, 1,1);

		Label nmLabel = new Label("Name: ");
		TextField nmField = new TextField();
		grid.add(nmLabel, 1, 1, 1, 1);
		grid.add(nmField, 2, 1, 1, 1);

		Label eLabel = new Label("Email: ");
		TextField eField = new TextField();
		grid.add(eLabel, 1,2,1,1);
		grid.add(eField, 2,2,1,1);

		Label pLabel = new Label("PhoneNo: ");
		TextField pField = new TextField();
		grid.add(pLabel, 1,3,1,1);
		grid.add(pField, 2,3,1,1);

		Button retButton = new Button("Return");
		Button submit = new Button("Submit");
		grid.add(retButton, 1,4,1,1);
		grid.add(submit, 2,4,1,1);

		retButton.setOnAction(e->_stage.setScene(_return));
		// Finally we'll deal with submitting of the data
		submit.setOnAction(e-> submitInstructor(
						idField.getText(), 
						nmField.getText(),
						eField.getText(),
						pField.getText()
						));

		Scene ret = new Scene(grid, 1280, 720);
		instructorInputScene = ret;
	}

}
