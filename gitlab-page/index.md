# Alejandro's Notes

Here you will find all the notes in reference book format below.

If some of this information is inaccurate or missing details please feel free to submit a merge request or contact me via Email/Discord:

* Email: alejandros714@protonmail.com

* Discord: shockrah#2647

* Public Repository: [gitlab.com/shockrah/csnotes](https://gitlab.com/shockrah/csnotes/)

[Intro to Networking](intro-to-networking-311.html)

[Networking Administration](network-administration-412.html)

[Networking and Security Concepts](network-security-concepts-312.html)

[Intro to Databases](intro-to-databases-363.html)

[Advanced Algorithms](advanced-algorithms-370.html)

[Computer Architecture with MIPS](computer-architecture-337.html)
