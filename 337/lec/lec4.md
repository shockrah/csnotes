# lec4

## Binary Addition

Let's say we want to add the binary numbers `0b0011` and `0b1001`.

To do this we have to consider what happens when we do `1+1`.
If we only have 1 bit of space to work with then our answer is just `0`.
In normal terms if we only have digit of space to work with 5+5 is also 0 but with a carry of 1.
Same deal in binary: `1+1=0 {Carry=1}`.

So now we have:

```
 11  <-- Carry row
0011 + 
1001
----
1100 <-- Final answer
```
## Binary Subtraction

Taking the problem `0011-1001` what we're actually going to do is find the 2's complement of the second number. 
This will be the negative version of that number which means its equivalent to saying `0011+(-1001)`.

So now we have basic addition but our `1001` becomes `0111`.

```
111  <-- carry bits
0011 +
0111
----
1010 <-- Final answer
```

Regardless of what happens we will always produce one special number alongside our result: _the carry bit_.
This is just the bit that carries out from the computation.
In both of our examples that bit would have been 0 but sometimes you'll notice that the carry is 1.
Both scenarios are valid depending on what data your adding/subtracting.
