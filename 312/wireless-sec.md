# Wireless Security

Let's go over the biggest most issue with wireless networks/communications

> Wireless signals must be broadcast

This means if _anything_ wants to communicate wirelessly then, everything/everyone will be able to hear those messages.
This also means that wireless receivers have to filter through a ton of noise/signals that aren't meant for it.

Taking the more innocent approach for a minute: it's kinda like walking around a crowded place, while talking to someone, you _can_ hear other people but mentally you are discarding other conversations as you're paying attention to the person you are talking to.

> DOS - Denial of Service

Furthering the crowded room example if you and a friend were talking and someone else started screaming talking to them would become much harder/impossible at some point.

