# lec13

## Lab Exercises

This lecture has a lab portion in `lab/` directory.
Directions are on `index-structures-lab.pdf` and `ordered-indexes-lab.pdf`.

## Indexing

To create an index we do:

```
create index indexName on targetTable(attrs);
```
We create an index based on some field, where we sort the entries in this index table.
Each entry then contains a pointer to each record in the target table.
Sorting the indexes allows us to search them _much faster_ than we could ever do on disk.

> What about collision?

Then we simply add a pointer to the index's list of associated pointers.

The biggest problem we have with indexing that if have a large number of entries then we would end up storing a huge number of indexes and pointers.
In order to avoid this, we don't take all of the entries.
Instead of taking all entries we take instead every other entry into our index or even every third.
This means that if we have a search that lands us inside one of the gaps we still search in a binary fashion but once we detect that we are we should search a _gap_ we linearly search through that gap.


## Clustering 

First let's recall that ideally our data entries in some table are physically located close to each other on disk _and_, are ordered somehow.

### Dense Clustering
### Sparser Clustering
