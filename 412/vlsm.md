# VLSM Examples

Say we have the IPs: 210.19.[14 -> 19].0/24

We want to create to networks which support this many hosts

## Part A
* A: 1000 hosts
* B: 500 hosts

Doing A first(because it's bigger):

Third octet in our IP has the bit pattern of `0000 11100`

If we want to support 1000 hosts we need to borrow the last two bits of that pattern: 

> __problem__: one of those is 1 which means we can't borrow it as its part of our given network address.

Thus this problem then can't be done given this information.

If we had been given x.x..12.0 we could do it because 12's bit pattern is `0000 1100`, which leaves the last two bits as 0's that we can use for the hosts.

## Part B

This time we need to support 500 hosts so we need 9 bits for the hosts. This time we can use `x.x.14.0` as our network address

Our mask is /23 `255.255.154.0`. This leaves our network address as `210.19.14.0`.

* The first host is x.x.14.1

* The last host is x.x.15.254

* Broadcast is x.x.15.255
