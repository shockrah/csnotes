# lec13

Let's break down what most/any program must have(executable):

> Data

We need a place to put variables and maybe some compile time constants so when we compile our program we can place those things here.

> Code

This section will contain all of our running code(_duh it's in the name_). If you write any functions, loops etc they will live here.

> Stack

This area is used heavily for functions. 
Any time we call a function we push a return address onto our stack for instance so that we know where to go back to when that function ends.

## Little Endian & Big Endian

Let's say we have the number `5431`.
Since the 5 is the _largest_ number here we'll say this number is in _big endian_ format but if we wrote backwards... we have `1345` we would say it's stored as _little endian_ because 1 is the _smallest_ number but its at the front.

For hex we do something very similar: take `0x45FA` as big endian.
Converting `0x45FA` to little endian we get `0xFA45`

> ??Wait that does seem right!?!

Here's the one caveat: _bytes aren't affected by little or big endianess_.
So just leave the bytes alone and reverse them from there.

## Significance of this

The reason why we care is because if you're going to be writing assembly code then you must understand these things to successfully create anything with assembly.
Even so this will give a deeper understanding of what goes on when something happens in your code.


